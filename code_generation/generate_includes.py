#! /usr/bin/python3

import argparse
from os import path
import pathlib

import generate_cpp

EXTENSION=".hpp"

def parse_args():
    '''
        Parses the terminal arguments

        Returns:
            [obj] The parsed terminal arguments, as a class
    '''
    parser = argparse.ArgumentParser(
        prog="generate_includes",
        description="Generates includes inside a directory"
    )

    parser.add_argument("-d", "--directory", required=True, help="The directory to produce include files in...")
    parser.add_argument("-i", "--ignore_sub_directories", action="store_true", help="Ignores any subdirectories when generating the include file...")
    parser.add_argument("-r", "--recursively", action="store_true", help="Indicates whether to recursively ad include files in the directory...")
    parser.add_argument("-s", "--starting_dir", required=True, help="The starting directory to start the include at")
    return parser.parse_args()

def check_if_directory_exists(directory):
    if not directory.exists() and directory.is_dir():
        raise FileNotFoundError("Error: Directory {} doesn't exist".format(str(directory)))

def generate_include_files(directory, starting_directory, recursively=True):

    file_name = directory/(directory.name + EXTENSION)

    files = [d for d in directory.iterdir() if (not (d.name.startswith(".")) and (d.is_file()))]
    sub_dirs = [d for d in directory.iterdir() if (not (d.name.startswith(".")) and (d.is_dir()))]
    
    if recursively:
        for sub_dir in sub_dirs:
            generate_include_files(sub_dir, starting_directory, recursively)

    # We are going to remove the part we don't care about in each filepath, to get down to the part we care about...
    starting_parent = str(starting_directory.parent)
    list_inc = []
    for file in files:
        if file.stem != directory.name:
            inc_str = str(file).replace(starting_parent+"/", "")
            list_inc.append(generate_cpp.generate_include_statement(inc_str))
    
    for sub_dir in sub_dirs:
        fn = sub_dir/(sub_dir.name + EXTENSION)
        if fn.exists():
            inc_str = str(fn).replace(starting_parent+"/", "")
            list_inc.append(generate_cpp.generate_include_statement(inc_str))
    
    # The filename generated will be the directory name, with the correct c++ extension...
    

    header_guard_statement = str(file_name).replace(starting_parent+"/", "")
    header_guard = generate_cpp.generate_header_guard(header_guard_statement)

    include_str = "\n".join(list_inc)

    file_contents = header_guard.format(file_contents=include_str)
    #import pdb; pdb.set_trace()

    file_name.write_text(file_contents)


def main(directory, starting_directory, recursively):
    
    starting_directory = pathlib.Path(starting_directory)
    check_if_directory_exists(starting_directory)
    starting_directory = starting_directory.absolute()

    directory = pathlib.Path(directory)
    check_if_directory_exists(directory)
    directory = directory.absolute()
    
    generate_include_files(directory, starting_directory, recursively)

if __name__ == "__main__":
    args = parse_args()
    main(args.directory, args.starting_dir, args.recursively)


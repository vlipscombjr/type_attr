

TAB = " " * 4
DEFAULT_LEVEL=0

def replace_namespace_delimiter(namespace_delimited_expr, replace_val="_"):
    '''
        Removes the namespace delimiter from a c++ expression, and replaces it
    '''
    if (namespace_delimited_expr.startswith("::")):
        namespace_delimited_expr = namespace_delimited_expr[2:]
    namespace_delimited_expr = namespace_delimited_expr.replace("::", replace_val)
    return namespace_delimited_expr

def generate_docstring(doc_dict, local_vars, global_vars, tab = TAB, level=DEFAULT_LEVEL):
    '''
        Generates the docstring for the given exopression

        Args:
            doc_dict [dict] The doc string to include
            tab [str] The string representation of the tab to use
            level [int] The indention level of the doc

        Returns:
            [str] Doc string for the given function/method
    '''
    doc = doc_dict["doc_str"]
    argz = None if "args" not in doc_dict else generate_args(doc_dict["args"], local_vars, global_vars)

    if not len(doc): return ""

    act_tab = tab*level
    pre_comment = "{} *".format(act_tab)

    doc_str  = "\n{}/**\n".format(act_tab)
    doc_str += pre_comment + "\n"
    for key, value in doc.items():
        doc_str += "{} @{} {}\n".format(pre_comment, key, value)
    doc_str += "{} */".format(act_tab)
    
    if argz is not None: doc_str = doc_str.format(**argz)
    return doc_str

def generate_statement(statement_dict, local_vars, global_vars, tab = TAB, level=DEFAULT_LEVEL):
    argz = None if "args" not in statement_dict else generate_args(statement_dict["args"], local_vars, global_vars)
    return statement_dict["code"] if argz is None else statement_dict["code"].format(**argz)

def generate_namespace_str(namespace, tab=TAB, level=DEFAULT_LEVEL):
    ''' 
        Generates the namespace string

        Args:
            namespace [str]: The name of the namespace (empty means no namespace is being generated)
            tab [str] The string representation of the tab to use
            level [int] The indention level of the doc

        Returns:
            [str] The namespace string to be used by python's format method

        Example:
            generate_namespace_str(test)
            output: 
                namespace test
                {{
                    {namespace_data}
                }}
            where the inner curly braces denote where the code inside the namespace goes
    '''
    if (namespace == ""):
        return "{}"
    else:
        return "{tab}namespace {namespace_name}\n{tab}{{{{\n{tab_1}{{namespace_data}}\n{tab}\n{tab}}}}}\n\n".format(namespace_name=namespace, tab=(tab*level), tab_1=(tab*(level+1)))



def get_local_or_global_var(var_type, value, local_vars, global_vars):
    if var_type.lower() == "local_variable":
        return local_vars[value]
    elif var_type.lower() == "global_variable":
        return global_vars[value]
    else:
        raise ValueError("Error: Invalid type \"{}\" when parsing for variables".format(var_type))


def generate_args(argz, local_vars, global_vars):
    '''
        Generates an argument dictionary
    '''
    new_argz = {}
    for arg in argz:
        arg_key = arg["key"]
        arg_type = arg["type"].lower()

        if arg_type == "value":
            new_argz[arg_key] = arg["value"]
        elif arg_type == "local_variable" or arg_type == "global_variable":
            arg_value = arg["value"]
            new_argz[arg_key] = get_local_or_global_var(arg_type, arg_value, local_vars, global_vars)

        elif arg_type == "expr":
            exprs = arg["exprs"]
            for expr_dict in exprs:
                
                rhs_expr = expr_dict["expr"]
                expr_args = generate_args(expr_dict["args"], local_vars, global_vars)
                rhs_expr = rhs_expr.format(**expr_args)
                lhs_expr = "new_argz[\"{}\"]".format(arg_key)
                expr = "{} = {}".format(lhs_expr, rhs_expr)
                exec(expr)

        else:
            raise KeyError("Invalid key type {}".format())
    return new_argz

def generate_namespace_file_data(namespaces, tab=TAB, level=DEFAULT_LEVEL):
    '''
        Generates the internals of the given file, given the list of namespaces from the YAML file

        Args:
            namespaces: The list of namespaces to add to the file
            tab [str] The string representation of the tab to use
            level [int] The indention level of the doc

        Returns:
            [str] The inner file data

    '''
    output_data = ""
    
    for namespace in namespaces:
        namespace_str = generate_namespace_str(namespace["name"], tab, level)
        new_level = level+1 if namespace_str != "{}" else level
        namespace_inner_data = ""

        for expr_type in namespace["types"]:
            statement = expr_type["statement"]

            # Create the doc string, and the inner method, for each type
            for list_elem in expr_type["list"]:
                doc_str = generate_docstring(expr_type["doc"], locals(), globals(), tab, new_level)
                inner_statement = generate_statement(expr_type["statement"], locals(), globals(), tab, new_level)
                expr = "{}\n{}\n".format(doc_str, inner_statement)
                namespace_inner_data += expr

        namespace_str = namespace_str.format(namespace_data=namespace_inner_data)
        output_data += namespace_str

    return output_data

def generate_header_guard(filename):
    '''
        Generates the header guard given the filename

        Args:
            filename: The name of the file

        Returns:
            [str] The header guard statement. 
    '''
    guard = filename.replace("/", "_").replace(".", "_").upper()
    header_guard = "#ifndef {guard}\n#define {guard}\n\n{{file_contents}}\n\n#endif // END {guard}".format(guard=guard)
    return header_guard

def generate_include_statements(list_includes):
    '''
        Generates a group of includes, given a list of includes

        Args:
            list_includes [list(str)]: The include statements to create
        
        Returns: 
            [str] The file includes
    '''
    include_statements = ""
    for include in list_includes:
        include_statements += generate_include_statement(include) + "\n"
    include_statements+="\n"
    return include_statements

def generate_include_statement(filename):
    '''
        Generates the header include statement given the filename

        Args: 
            filename: The path to the file, referenced to the root directory

        Returns:
            [str] An include statement. 
    
    '''
    inc = filename.replace("include/", "", 1)
    inc = "#include <{inner_include_statement}>".format(inner_include_statement=inc)
    return inc


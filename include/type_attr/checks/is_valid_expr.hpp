#ifndef INCLUDE_TYPE_ATTR_IS_VALID_EXPR
#define INCLUDE_TYPE_ATTR_IS_VALID_EXPR

#include <type_traits>

namespace ta
{
    namespace internal
    {
        /**
         * @brief Determines if a given input expression is valid or not
         * 
         * @tparam Expr The expression class 
         * @tparam Enabled Used for SFINAE
         * @tparam Args Argument types to pass into the expression class
         */
        template <template <typename...> typename Expr, typename Enabled=void, typename... Args> struct is_valid_expr : std::false_type {};
        
        /**
         * @brief Determines if a given input expression is valid or not
         * 
         * @tparam Expr The expression class 
         * @tparam Args Argument types to pass into the expression class
         */
        template <template <typename...> typename Expr, typename... Args> 
        struct is_valid_expr
        <
            Expr, 
            std::conditional_t <false, std::void_t<Expr<Args...>>, void>,
            Args...
        > : std::true_type {};
    }

    /**
     * @brief Determines if a given input expression is valid or not
     * 
     * @tparam Expr The expression class 
     * @tparam Args Argument types to pass into the expression class
     */
    template <template <typename...> typename Expr, typename... Args> 
    using is_valid_expr = internal::is_valid_expr<Expr, void, Args...>;

    /**
     * @brief Determines if a given input expression is valid or not
     * 
     * @tparam Expr The expression class 
     * @tparam Args Argument types to pass into the expression class
     */
    template <template <typename...> typename Expr, typename... Args> inline constexpr bool is_valid_expr_v = is_valid_expr<Expr, Args...>::value;
}

#endif /* INCLUDE_TYPE_ATTR_IS_VALID_EXPR */

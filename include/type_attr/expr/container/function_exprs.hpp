
/**
 *
 * @file function_exprs.hpp
 * @author Vernard Lipscomb Jr <vlipscombjr@gmail.com>
 * @brief Contains function expressions used to determine if a class contains a particular container method
 * @details IMPORTANT NOTE - DO NOT EDIT THIS FILE DIRECTLY... This is an automatically generated file
 */

#ifndef TYPE_ATTR_EXPR_CONTAINER_FUNCTION_EXPRS_HPP
#define TYPE_ATTR_EXPR_CONTAINER_FUNCTION_EXPRS_HPP

#include <type_traits>


namespace ta::container
{
    
    /**
     *
     * @brief Expression determines if function begin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using begin_fexpr = decltype(begin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function end is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using end_fexpr = decltype(end(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function cbegin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using cbegin_fexpr = decltype(cbegin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function cend is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using cend_fexpr = decltype(cend(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function rbegin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using rbegin_fexpr = decltype(rbegin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function rend is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using rend_fexpr = decltype(rend(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function crbegin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using crbegin_fexpr = decltype(crbegin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function crend is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using crend_fexpr = decltype(crend(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function size is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using size_fexpr = decltype(size(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function len is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using len_fexpr = decltype(len(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function length is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using length_fexpr = decltype(length(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function at is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using at_fexpr = decltype(at(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function data is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using data_fexpr = decltype(data(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if function find is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using find_fexpr = decltype(find(::std::declval<Args>()...));


}

namespace ta::container
{
    
    /**
     *
     * @brief Expression determines if standard template library function ::std::begin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_begin_fexpr = decltype(::std::begin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::end is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_end_fexpr = decltype(::std::end(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::cbegin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_cbegin_fexpr = decltype(::std::cbegin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::cend is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_cend_fexpr = decltype(::std::cend(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::rbegin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_rbegin_fexpr = decltype(::std::rbegin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::rend is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_rend_fexpr = decltype(::std::rend(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::crbegin is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_crbegin_fexpr = decltype(::std::crbegin(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::crend is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_crend_fexpr = decltype(::std::crend(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::size is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_size_fexpr = decltype(::std::size(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::data is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_data_fexpr = decltype(::std::data(::std::declval<Args>()...));

    /**
     *
     * @brief Expression determines if standard template library function ::std::find is callable given input parameters
     */
    template<typename Cls, typename... Args> 
    using std_find_fexpr = decltype(::std::find(::std::declval<Args>()...));


}





#endif // END TYPE_ATTR_EXPR_CONTAINER_FUNCTION_EXPRS_HPP
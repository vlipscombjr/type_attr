#ifndef TYPE_ATTR_EXPR_MATH_MATH_HPP
#define TYPE_ATTR_EXPR_MATH_MATH_HPP

#include <type_attr/expr/math/function_exprs.hpp>
#include <type_attr/expr/math/method_exprs.hpp>

#endif // END TYPE_ATTR_EXPR_MATH_MATH_HPP
#ifndef INCLUDE_TYPE_ATTR_TYPE_TYPE
#define INCLUDE_TYPE_ATTR_TYPE_TYPE

#include <climits>
#include <iostream>
#include <limits>
#include <string>
#include <string_view>
#include <type_traits>

#include <type_attr/expr/expr.hpp>
#include <type_attr/checks/checks.hpp>
#include <type_attr/util/type_name.hpp>


/** 
 * TODO: Make a recursive template type for the following:
 *      - addPointer
 * 
 */

namespace ta
{
    template <typename T = void>
    struct Type
    {
        Type() {}
        Type(T t) {}

        using type = T;

        using addConst = Type<std::add_const_t<T>>;
        using addConstVolatile = Type<std::add_cv_t<T>>;
        using addLValueReference = Type<std::add_lvalue_reference_t<T>>;
        using addPointer = Type<std::add_pointer_t<T>>;
        using addRValueReference = Type<std::add_rvalue_reference_t<T>>;
        using addVolatile = Type<std::add_volatile_t<T>>;
        
        using removeConst = Type<std::remove_const_t<T>>;
        using removeConstVolatile = Type<std::remove_cv_t<T>>;
        using removePointer = Type<std::remove_pointer_t<T>>;
        using removeReference = Type<std::remove_reference_t<T>>;
        using removeVolatile = Type<std::remove_volatile_t<T>>;

        template <typename U> static inline constexpr auto canAdd() -> bool;
        template <typename U> static inline constexpr auto canAdd(U u) -> bool;

        template <template <typename...> typename Expr, typename... Args> static inline constexpr auto canApply() -> bool { return is_valid_expr_v<Expr, Args...>; }

        static inline constexpr auto hasUniqueObjectRepresentations() -> bool { return std::has_unique_object_representations_v<T>; }
        static inline constexpr auto hasVirtualDestructor() -> bool { return std::has_virtual_destructor_v<T>; }

        static inline constexpr auto isAbstract() -> bool { return std::is_abstract_v<T>; }
        static inline constexpr auto isAggregate() -> bool { return std::is_aggregate_v<T>; }
        static inline constexpr auto isArithmetic() -> bool { return std::is_arithmetic_v<T>; }
        static inline constexpr auto isBool() -> bool { return std::is_same_v<T, bool>; }
        static inline constexpr auto isCArray() -> bool { return std::is_array_v<T>; }
        static inline constexpr auto isClass() -> bool { return std::is_class_v<T>; }
        static inline constexpr auto isConst() -> bool { return std::is_const_v<T>; }
        static inline constexpr auto isCompound() -> bool { return std::is_compound_v<T>; }
        static inline constexpr auto isEmpty() -> bool { return std::is_empty_v<T>; }
        static inline constexpr auto isEnum() -> bool { return std::is_enum_v<T>; }
        static inline constexpr auto isFinal() -> bool { return std::is_final_v<T>; }
        static inline constexpr auto isFloatingPoint() -> bool { return std::is_floating_point_v<T>; }
        static inline constexpr auto isFundamental() -> bool { return std::is_fundamental_v<T>; }
        static inline constexpr auto isFunction() -> bool { return std::is_function_v<T>; }
        static inline constexpr auto isIntegral() -> bool { return std::is_integral_v<T>; }
        static inline constexpr auto isLValueReference() -> bool { return std::is_lvalue_reference_v<T>; }
        static inline constexpr auto isMemberPointer() -> bool { return std::is_member_pointer_v<T>; }
        static inline constexpr auto isMemberObjectPointer() -> bool { return std::is_member_object_pointer_v<T>; }
        static inline constexpr auto isMemberFunctionPointer() -> bool { return std::is_member_function_pointer_v<T>; }
        static inline constexpr auto isNullPtr() -> bool { return std::is_null_pointer_v<T>; }
        static inline constexpr auto isObject() -> bool { return std::is_object_v<T>; }
        static inline constexpr auto isPointer() -> bool { return std::is_pointer_v<T>; }
        static inline constexpr auto isPolymorphic() -> bool { return std::is_polymorphic_v<T>; }
        static inline constexpr auto isReference() -> bool { return std::is_reference_v<T>; }
        static inline constexpr auto isRValueReference() -> bool { return std::is_rvalue_reference_v<T>; }
        static inline constexpr auto isSigned() -> bool { return std::is_signed_v<T>; }
        static inline constexpr auto isStandardLayout() -> bool { return std::is_standard_layout_v<T>; }
        static inline constexpr auto isTrivial() -> bool { return std::is_trivial_v<T>; }
        static inline constexpr auto isTriviallyCopyable() -> bool { return std::is_trivially_copyable_v<T>; }
        static inline constexpr auto isUnion() -> bool { return std::is_union_v<T>; }
        static inline constexpr auto isUnsigned() -> bool { return std::is_unsigned_v<T>; }
        static inline constexpr auto isVoid() -> bool { return std::is_void_v<T>; }
        static inline constexpr auto isVolatile() -> bool { return std::is_volatile_v<T>; }

        template <typename U> static inline constexpr auto isSame() -> bool { return std::is_same_v<T, U>; }
        template <typename U> static inline constexpr auto isSame(U u) -> bool { return Type<T>::isSame<U>(); }

        static inline constexpr auto repr() { return Type<Type<T>>().toString() + "()"; }

        static inline constexpr auto size() -> std::size_t { return sizeof(T); }
        static inline constexpr auto sizeBits() -> std::size_t { return sizeof(T) * CHAR_BIT; }

        static inline constexpr auto toCString() -> char* { return std::string(type_name<T>()).c_str(); }
        static inline constexpr auto toString() -> std::string { return std::string(type_name<T>()); }
        static inline constexpr auto toStringView() -> std::string_view { return type_name<T>(); }

        template <typename... Args, std::enable_if_t<(sizeof...(Args) > 0), bool> = true> static inline constexpr auto isConstructable() -> bool { return std::is_constructible_v<T, Args...>; }
        template <typename... Args> static inline constexpr auto isConstructable(Args... args) -> bool { return std::is_constructible_v<T, Args...>; } 
        static inline constexpr auto isDefaultConstructable() -> bool { return std::is_default_constructible_v<T>; } 
        static inline constexpr auto isCopyConstructable() -> bool { return std::is_copy_constructible_v<T>; } 
        static inline constexpr auto isMoveConstructable() -> bool { return std::is_move_constructible_v<T>; } 
        static inline constexpr auto isDestructible() -> bool { return std::is_destructible_v<T>; } 
        template <typename U> static inline constexpr auto isAssignable() -> bool { return std::is_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isAssignable(U u) -> bool { return Type<T>::isAssignable<U>(); }
        template <typename U> static inline constexpr auto isCopyAssignable() -> bool { return std::is_copy_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isCopyAssignable(U u) -> bool { return Type<T>::isCopyAssignable<U>(); }
        template <typename U> static inline constexpr auto isMoveAssignable() -> bool { return std::is_move_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isMoveAssignable(U u) -> bool { return Type<T>::isMoveAssignable<U>(); }

        template <typename... Args, std::enable_if_t<(sizeof...(Args) > 0), bool> = true> static inline constexpr auto isNoThrowConstructable() -> bool { return std::is_nothrow_constructible_v<T, Args...>; }
        template <typename... Args> static inline constexpr auto isNoThrowConstructable(Args... args) -> bool { return std::is_nothrow_constructible_v<T, Args...>; } 
        static inline constexpr auto isNoThrowDefaultConstructable() -> bool { return std::is_nothrow_default_constructible_v<T>; } 
        static inline constexpr auto isNoThrowCopyConstructable() -> bool { return std::is_nothrow_copy_constructible_v<T>; } 
        static inline constexpr auto isNoThrowMoveConstructable() -> bool { return std::is_nothrow_move_constructible_v<T>; } 
        static inline constexpr auto isNoThrowDestructible() -> bool { return std::is_nothrow_destructible_v<T>; } 
        template <typename U> static inline constexpr auto isNoThrowAssignable() -> bool { return std::is_nothrow_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isNoThrowAssignable(U u) -> bool { return Type<T>::isNoThrowAssignable<U>(); }
        template <typename U> static inline constexpr auto isNoThrowCopyAssignable() -> bool { return std::is_nothrow_copy_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isNoThrowCopyAssignable(U u) -> bool { return Type<T>::isNoThrowCopyAssignable<U>(); }
        template <typename U> static inline constexpr auto isNoThrowMoveAssignable() -> bool { return std::is_nothrow_move_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isNoThrowMoveAssignable(U u) -> bool { return Type<T>::isNoThrowMoveAssignable<U>(); }

        template <typename... Args, std::enable_if_t<(sizeof...(Args) > 0), bool> = true> static inline constexpr auto isTriviallyConstructable() -> bool { return std::is_trivially_constructible_v<T, Args...>; }
        template <typename... Args> static inline constexpr auto isTriviallyConstructable(Args... args) -> bool { return std::is_trivially_constructible_v<T, Args...>; } 
        static inline constexpr auto isTriviallyDefaultConstructable() -> bool { return std::is_trivially_default_constructible_v<T>; } 
        static inline constexpr auto isTriviallyCopyConstructable() -> bool { return std::is_trivially_copy_constructible_v<T>; } 
        static inline constexpr auto isTriviallyMoveConstructable() -> bool { return std::is_trivially_move_constructible_v<T>; } 
        static inline constexpr auto isTriviallyDestructible() -> bool { return std::is_trivially_destructible_v<T>; } 
        template <typename U> static inline constexpr auto isTriviallyAssignable() -> bool { return std::is_trivially_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isTriviallyAssignable(U u) -> bool { return Type<T>::isTriviallyAssignable<U>(); }
        template <typename U> static inline constexpr auto isTriviallyCopyAssignable() -> bool { return std::is_trivially_copy_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isTriviallyCopyAssignable(U u) -> bool { return Type<T>::isTriviallyCopyAssignable<U>(); }
        template <typename U> static inline constexpr auto isTriviallyMoveAssignable() -> bool { return std::is_trivially_move_assignable_v<T, U>; }
        template <typename U> static inline constexpr auto isTriviallyMoveAssignable(U u) -> bool { return Type<T>::isTriviallyMoveAssignable<U>(); }

        template <typename U> static inline constexpr auto isSwappableWith() -> bool { return std::is_swappable_with_v<T, U>; }
        template <typename U> static inline constexpr auto isSwappableWith(U u) -> bool { return Type<T>::isSwappableWith<U>(); }
        template <typename U> static inline constexpr auto isNoThrowSwappableWith() -> bool { return std::is_nothrow_swappable_with_v<T, U>; }
        template <typename U> static inline constexpr auto isNoThrowSwappableWith(U u) -> bool { return Type<T>::isNoThrowSwappableWith<U>(); }
        static inline constexpr auto isSwappable() -> bool { return std::is_swappable_v<T>; }
        static inline constexpr auto isNoThrowSwappable() -> bool { return std::is_nothrow_swappable_v<T>; }

        // TODO: Check to see if recursion is needed to check further down the chain...
        template <typename Derived> static inline constexpr auto isBaseOf() -> bool { return std::is_base_of_v<T, Derived>; }
        template <typename Derived> static inline constexpr auto isBaseOf(Derived d) -> bool { return Type<T>::isBaseOf<Derived>(); }
        template <typename Base> static inline constexpr auto isDerivedFrom() -> bool { return std::is_base_of_v<Base, T>; }
        template <typename Base> static inline constexpr auto isDerivedFrom(Base b) -> bool { return Type<T>::isDerivedFrom<Base>(); }

        template <typename U> static inline constexpr auto isConvertibleTo() -> bool { return std::is_convertible_v<T, U>; }
        template <typename U> static inline constexpr auto isConvertibleTo(U u) -> bool { return Type<T>::isConvertibleTo<U>(); }
        template <typename U> static inline constexpr auto isConvertibleFrom() -> bool { return std::is_convertible_v<U, T>; }
        template <typename U> static inline constexpr auto isConvertibleFrom(U u) -> bool { return Type<T>::isConvertibleFrom<U>(); }

        // TODO: Add variants
        template <typename... Args> static inline constexpr auto isInvocable() -> bool { return std::is_invocable_v<T, Args...>; }

        template<typename U> inline constexpr auto operator== (U) -> bool { return std::is_same_v<T, U>; }
        template<typename U> inline constexpr auto operator== (Type<U>) -> bool { return std::is_same_v<T, U>; }

    };

    /**
     * @brief Allows for the Type struct to be printable with cout
     * 
     * @tparam T The type associated with Type
     * @param out The ostream 
     * @param type The type to output
     * @return std::ostream& The output ostream
     */
    template <typename T>
    std::ostream& operator << (std::ostream& out, Type<T> const& type)
    {
        out << type.toStringView();
        return out;
    }


}


#endif /* INCLUDE_TYPE_ATTR_TYPE_TYPE */

#ifndef INCLUDE_TYPE_ATTR_TYPE_NUMERICAL_TYPE
#define INCLUDE_TYPE_ATTR_TYPE_NUMERICAL_TYPE

#include <complex>

#include <type_attr/type/type.hpp>

namespace ta
{
    template <typename T>
    struct NumericalType : public Type<T>
    {

        // TODO: Make these work for fundamental types, basic containers where type is first, 
        template <template<typename> typename U = std::complex> using makeComplex = T;
        using makeReal = T;

        
        static inline constexpr auto behavesAsInteger() -> bool;
        static inline constexpr auto behavesAsFloatingPoint() -> bool;

        
    };
}



#endif /* INCLUDE_TYPE_ATTR_TYPE_NUMERICAL_TYPE */

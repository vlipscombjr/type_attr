#ifndef INCLUDE_TYPE_ATTR_UTIL_TYPE_NAME
#define INCLUDE_TYPE_ATTR_UTIL_TYPE_NAME

#include <string_view>
#include <type_traits>

namespace ta
{
    /**
     * @brief Obtains the string representation of the datatype
     * 
     * @tparam T The type to obtain the name from
     * 
     * @return A string view of the datatype
     * 
     * Code was found from stack overflow, here:
     *  
     */
    template <typename T>
    constexpr auto type_name()
    {
        std::string_view name, prefix, suffix;
        #if defined(__GNUC__)
            name = __PRETTY_FUNCTION__;
            prefix = "constexpr auto ta::type_name() [with T = ";
            suffix = "]";

        #elif defined(__clang__)
            name = __PRETTY_FUNCTION__;
            prefix = "auto ta::type_name() [T = ";
            suffix = "]";
        
        #elif defined(_MSC_VER)
            name = __FUNCSIG__;
            prefix = "auto __cdecl ta::type_name<";
            suffix = ">(void)";
        #else   
            static_assert(true, "Error: Compiler in use is not supported...");
        #endif
        name.remove_prefix(prefix.size());
        name.remove_suffix(suffix.size());
        return name;
    }

    /**
     * @brief Obtains the string 
     * 
     * @tparam T The type to obtain the name from
     * 
     * @param 
     * 
     * @return constexpr auto A string view of the datatype
     */
    template <typename T>
    constexpr auto type_name(T t) 
    {
        return type_name<T>();
    }
}

#endif /* INCLUDE_TYPE_ATTR_UTIL_TYPE_NAME */

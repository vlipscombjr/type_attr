
#include <iostream>

#include <type_attr/util/type_name.hpp>

#include <type_attr/type/type.hpp>
#include <type_attr/type/numerical_type.hpp>

#include <type_attr/expr/expr.hpp>
#include <type_attr/checks/is_valid_expr.hpp>


struct Dummy
{
    int abs() {return 1;}
};

int main()
{
    using t = ta::Type<decltype(1)>::addPointer;

    auto a = 5;
    auto b = 10.0f;

    ta::NumericalType<int>::type c = a+b;

    std::cout << (ta::Type(a) == ta::Type(b)) << "\n";
    std::cout << (ta::Type(a).size() == ta::Type(b).size()) << "\n";
    std::cout << ta::NumericalType<int>() << "\n";

    std::cout << ta::is_valid_expr_v<ta::math::cos_mexpr, Dummy> << "\n";
    return 0; 
}